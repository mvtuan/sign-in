## My assignment
**Download project and run app with 'npm start' command**
**Total work**
1. Login (100%)
-   Created login page that is similar to mock up
-   Validate username using Regex
-   Validate password using Regex
-   Login function (In order to use API I ignore validation function and use default user info given in the API tutorial)

2. Register (100%)
-   Created login page that is similar to mock up
-   Validate username using Regex
-   Validate password using Regex
-   Validate email using Regex
-   Compare passwords
-   Register function (In order to use API I ignore validation function and use default user info given in the API tutorial)

3. Forgot password (95%)
-   Created login page that is similar to mock up
-   Send forgot password request
-   Validate OTP code

**Difficulties**
-   Unknown user info after logged in. Therefore, I couldn't do show user info
-   OTP inputs is not completed as wanted
-   Unable to send reset password request due to expired token
