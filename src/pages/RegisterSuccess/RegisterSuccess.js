import React from 'react'
import './RegisterSuccess.scss'
import { Link } from 'react-router-dom'

const RegisterSuccess = () => {
    return (
      <div className="container">
        <div className="register-success">
          <div className="tick-icon">
            <i class="fas fa-check"></i>
          </div>
          <div className='shadow'></div>
          <h3 className="title">Đăng ký thành công</h3>
          <p>Cảm ơn bạn đã hoàn thành thông tin đăng ký.</p>
          <Link className="btn-redirect" to='/'>Về trang chủ</Link>
        </div>
      </div>
    );
}

export default RegisterSuccess
