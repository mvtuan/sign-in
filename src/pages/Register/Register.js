import React, { useState, useEffect } from "react";
import "./Register.scss";
import { useHistory, useLocation } from "react-router-dom";
import axios from "axios";

const Register = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [gender, setGender] = useState("");
  const [experience, setExperience] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [passwordVisibility, setPasswordVisibility] = useState(false);
  const [confirmPasswordVisibility, setConfirmPasswordVisibility] =
    useState(false);
  const [data, setData] = useState();

  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    if (data) {
      history.push(location.search + '/register-success');
    }
  }, [location, history, data]);

  // Data validation
  const validUser = new RegExp("^[a-zA-Z0-9]+(?:[_ .]?[a-zA-Z0-9]).{3,}$");
  const validPassword = new RegExp(
    "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%_]).{8,}$"
  );
  const validEmail = new RegExp(
    "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
  );
  const validateUser = () => {
    if (validUser.test(username)) {
      return true;
    } else {
      return false;
    }
  };

  const validatePassword = () => {
    if (validPassword.test(password)) {
      return true;
    } else {
      return false;
    }
  };

  const validateEmail = () => {
    if (validEmail.test(email)) {
      return true;
    } else {
      return false;
    }
  };

  const comparePassword = () => {
    if(password === confirmPassword)
      return true;
      return false;
  }

  const submitHandler = (e) => {
    e.preventDefault();
    if (!validateUser()) {
      console.log("Invalid username");
    }
    if (!validatePassword()) {
      console.log("Invalid password");
    }
    if (!validateEmail()) {
      console.log("Invalid email");
    }
    if (!comparePassword()) {
      console.log("Password do not match");
    }

    if(comparePassword()){
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      axios
        .post(
          "https://api.v2-dev.thuocsi.vn/interview/account",
          {
            username,
            displayName,
            email,
            phone,
            gender,
            experience,
            password,
          },
          config
        )
        .then((res) => {
          console.log(res.data);
          setData(res.data);
        })
        .catch((err) => {
          console.log(err);
        });

    }
  };
  return (
    <div className="container">
      <div className="register">
        <h3 className="register__title">Đăng ký người dùng</h3>
        <form className="register__form" onSubmit={submitHandler}>
          <label>
            Tên đăng nhập <span className="required-mark">*</span>
          </label>
          <input
            className="register__input"
            type="text"
            required
            placeholder="Nhập tên đăng nhập"
            onChange={(e) => setUsername(e.target.value)}
          />
          <div className="password-section">
            <div className="password-section__item">
              <label>
                Mật khẩu <span className="required-mark">*</span>
              </label>
              <input
                className="register__input"
                type={passwordVisibility ? "text" : "password"}
                required
                placeholder="Nhập mật khẩu"
                onChange={(e) => setPassword(e.target.value)}
              />
              <span className="btn-toggle-visibility">
                <i
                  className={
                    passwordVisibility ? "far fa-eye-slash" : "far fa-eye"
                  }
                  onClick={() => setPasswordVisibility(!passwordVisibility)}
                ></i>
              </span>
            </div>
            <div className="password-section__item">
              <label>
                Nhập lại mật khẩu <span className="required-mark">*</span>
              </label>
              <input
                className="register__input"
                type={confirmPasswordVisibility ? "text" : "password"}
                required
                placeholder="Nhập lại mật khẩu"
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
              <span className="btn-toggle-visibility">
                <i
                  className={
                    confirmPasswordVisibility
                      ? "far fa-eye-slash"
                      : "far fa-eye"
                  }
                  onClick={() =>
                    setConfirmPasswordVisibility(!confirmPasswordVisibility)
                  }
                ></i>
              </span>
            </div>
          </div>
          <label>
            Giới tính <span className="required-mark">*</span>
          </label>
          <select
            className="gender"
            onChange={(e) => setGender(e.target.value)}
          >
            <option value="">--</option>
            <option value="MALE">Nam</option>
            <option value="FEMALE">Nữ</option>
          </select>
          <label id="experience-label">Bạn là:</label>
          <div className="experience-section">
            <div className="experience-section__item">
              <label for="intern">
                <input
                  type="radio"
                  id="intern"
                  name="exp"
                  value="INTERN"
                  onChange={(e) => setExperience(e.target.value)}
                />
                <span class="check-mark"></span>
                Thực tập sinh{" "}
              </label>
            </div>
            <div className="experience-section__item">
              <label for="fresher">
                <input
                  type="radio"
                  id="fresher"
                  name="exp"
                  value="FRESHER"
                  onChange={(e) => setExperience(e.target.value)}
                />
                <span class="check-mark"></span>
                Mới ra trường
              </label>
            </div>
            <div className="experience-section__item">
              <label for="junior">
                <input
                  type="radio"
                  id="junior"
                  name="exp"
                  value="JUNIOR"
                  onChange={(e) => setExperience(e.target.value)}
                />
                <span class="check-mark"></span>Đã có kinh nghiệm
              </label>
            </div>
          </div>
          <label>
            Tên hiển thị <span className="required-mark">*</span>
          </label>
          <input
            className="register__input"
            type="text"
            required
            placeholder="Nhập tên hiển thị"
            onChange={(e) => setDisplayName(e.target.value)}
          />
          <div className="contact-section">
            <div className="contact-section__item">
              <label>
                Email <span className="required-mark">*</span>
              </label>
              <input
                className="register__input"
                type="text"
                required
                placeholder="Nhập email"
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="contact-section__item">
              <label>
                Số địện thoại <span className="required-mark">*</span>
              </label>
              <input
                className="register__input"
                type="text"
                placeholder="Nhập số địện thoại"
                onChange={(e) => setPhone(e.target.value)}
              />
            </div>
          </div>
          <button className="register__button" type="submit">
            Đăng ký
          </button>
        </form>
      </div>
    </div>
  );
};

export default Register;
