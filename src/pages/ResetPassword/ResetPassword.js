import React, { useState, useEffect } from 'react'
import './ResetPassword.scss'
import { useHistory, useLocation } from "react-router-dom";
import axios from 'axios'

const ResetPassword = () => {
    const [actualPassword, setActualPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');
    const [newPasswordVisible, setNewPasswordVisible] = useState(false);
    const [confirmNewPasswordVisible, setConfirmNewPasswordVisible] = useState(false);
    const [data, setData] = useState();

      const location = useLocation();
      const history = useHistory();

    useEffect(() => {
      if(data) {
        history.push(location.search + '/reset-success')
      }
    }, [data, history, location])

    const validPassword = new RegExp(
      "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%_]).{8,}$"
    );

    const validatePassword = () => {
      if (validPassword.test(newPassword)) {
        return true;
      } else {
        return false;
      }
    };

      const comparePassword = () => {
        if (newPassword === confirmNewPassword) return true;
        return false;
      };

    const submitHandler = e => {
        e.preventDefault();
        if(!validatePassword()) console.log('Invalid password')
        if(!comparePassword()){
          console.log('Passwords do not match');
          return;
        } else {
          const email = "Email";
          setActualPassword("NewPassword");
          const token = "NjBiNjc1NWUwNmQ5Y2FmMzFkNjZiZWM5LjE2MjI1NzQ0MzU=";
          const config = {
            headers: {
              "Content-Type": "application/json",
            },
          };
          console.log(newPassword);
          axios.put(
            "https://api.v2-dev.thuocsi.vn/interview/account/forgot-password/password",
            { email, actualPassword, token}, config
          ).then(res => {
            console.log(res.data)
            setData(res.data);
          }).catch(err => {
            console.log(err);
          });

        }
    }
    return (
      <div className="container">
        <div className="reset-password">
          <form className="reset-form" onSubmit={submitHandler}>
            <h3 className="reset-form__title">Tạo lại mật khẩu</h3>
            <p>Vui lòng nhập khẩu mới</p>
            <label>
              Mật khẩu mới
              <span className="btn-toggle-visibility">
                <i
                  className={
                    newPasswordVisible ? "far fa-eye-slash" : "far fa-eye"
                  }
                  onClick={() => setNewPasswordVisible(!newPasswordVisible)}
                ></i>
              </span>
            </label>
            <input
              type={newPasswordVisible ? "text" : "password"}
              placeholder="Nhập mật khẩu mới"
              required
              onChange={(e) => setNewPassword(e.target.value)}
            />

            <label>
              Nhập lại mật khẩu mới
              <span className="btn-toggle-visibility">
                <i
                  className={
                    confirmNewPasswordVisible
                      ? "far fa-eye-slash"
                      : "far fa-eye"
                  }
                  onClick={() =>
                    setConfirmNewPasswordVisible(!confirmNewPasswordVisible)
                  }
                ></i>
              </span>
            </label>
            <input
              type={confirmNewPasswordVisible ? "text" : "password"}
              placeholder="Nhập lại mật khẩu mới"
              required
              onChange={(e) => setConfirmNewPassword(e.target.value)}
            />

            <button className="btn-submit" type="submit">
              Hoàn tất
            </button>
          </form>
        </div>
      </div>
    );
}

export default ResetPassword
