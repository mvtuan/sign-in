import React from 'react'
import './ResetPasswordSuccess.scss'
import { Link } from 'react-router-dom'

const ResetPasswordSuccess = () => {
    return (
      <div className="container">
        <div className="reset-password-success">
          <div className="tick-icon">
            <i class="fas fa-check"></i>
          </div>
          <div className="shadow"></div>
          <h3 className="title">Đặt lại mật khẩu thành công! </h3>
          <p>
            Tuyệt vời, bạn vừa đặt lại mật khẩu thành công. Vui lòng đăng nhập
            lại để tiếp tục sử dụng.
          </p>
          <Link className="btn-redirect" to="/login">
            Đăng nhập
          </Link>
        </div>
      </div>
    );
}

export default ResetPasswordSuccess
