import React, { useState, useEffect } from "react";
import "./Login.scss";
import { Link, useHistory, useLocation } from "react-router-dom";
import axios from "axios";


const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [data, setData] = useState();

  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    if(data) {
      history.push(location.search);
    }
  }, [location, history, data])
  
  const validUser = new RegExp("^[a-zA-Z0-9]+(?:[_ .]?[a-zA-Z0-9]).{3,}$");
  const validPassword = new RegExp(
    "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%_]).{8,}$"
  );
  const validateUser = () => {
    if (validUser.test(username)) {
      return true;
    } else {
      return false;
    }
  }

  const validatePassword = () => {
    if (validPassword.test(password)) {
      return true;
    } else {
      return false;
    }
  }

  const submitHandler = (e) => {
    e.preventDefault();
    if(!validateUser()) {
      console.log('Invalid username');
    }
    if(!validatePassword()) {
      console.log('Invalid password')
    }

    // if(validateUser() && validatePassword()){
    //   const config = {
    //     headers: {
    //       "Content-Type": "application/json",
    //     },
    //   };
    //   axios.post(
    //     "https://api.v2-dev.thuocsi.vn/interview/authorization",
    //     { username, password },
    //     config
    //     ).then(res => {
    //       setData(res.data);
    //       console.log(res.data)
          
    //     }).catch(err => {
    //       console.log(err)
    //     });

    // }
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    axios
      .post(
        "https://api.v2-dev.thuocsi.vn/interview/authorization",
        { username, password },
        config
      )
      .then((res) => {
        setData(res.data);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });

    
  };
  return (
    <div className="container">
      <div className="login">
        <h3 className="login__title">Đăng nhập</h3>
        <form className="login__form" onSubmit={submitHandler}>
          <label>Tên đăng nhập</label>
          <input
            className="login__input"
            type="text"
            placeholder="Nhập tên đăng nhập"
            required
            onChange={(e) => setUsername(e.target.value)}
          />
          <label>Mật khẩu</label>
          <input
            className="login__input"
            type={passwordVisible ? "text" : "password"}
            placeholder="Nhập mật khẩu"
            required
            onChange={(e) => setPassword(e.target.value)}
          />
          <span className="btn-toggle-visibility">
            <i
              className={passwordVisible ? "far fa-eye-slash" : "far fa-eye"}
              onClick={() => setPasswordVisible(!passwordVisible)}
            ></i>
          </span>
          <div className="remember-me">
            <label className="keep-sign-in" for="rememberMe">
              <input type="checkbox" value="lsRememberMe" id="rememberMe" />{" "}
              <span className="check-mark"></span>
              Giữ tôi luôn đăng nhập
            </label>
            <Link to="/forgot-password" className="link">
              Quên mật khẩu?
            </Link>
          </div>
          <button className="login__button" type="submit">
            Đăng nhập
          </button>
        </form>
        <p>
          Bạn chưa có tài khoản?{" "}
          <Link to="/register" className="link">
            Đăng ký
          </Link>
        </p>
      </div>
    </div>
  );
};

export default Login;
