import React, { useState, useEffect } from "react";
import "./OtpConfirm.scss";
import { Link, useHistory, useLocation } from "react-router-dom";
import axios from "axios";

const OtpConfirm = () => {
  const [firstNumber, setFirstNumber] = useState("");
  const [secondNumber, setSecondNumber] = useState("");
  const [thirdNumber, setThirdNumber] = useState("");
  const [forthNumber, setForthNumber] = useState("");
  const [fifthNumber, setFifthNumber] = useState("");
  const [otp, setOtp] = useState("");
  const [data, setData] = useState("");

  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    if (data) {
      history.push(location.search + '/reset-password')
    }
  }, [data, history, location]);
  const submitHandler = (e) => {
    e.preventDefault();
    const res = parseInt(
      firstNumber + secondNumber + thirdNumber + forthNumber + fifthNumber
    );
    setOtp(res);
    const email = "Email";
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    console.log(otp);
    axios
      .put(
        "https://api.v2-dev.thuocsi.vn/interview/account/forgot-password/otp",
        { email, otp },
        config
      )
      .then((res) => {
        setData(res.data);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="container">
      <div className="forgot-password">
        <form className="form" onSubmit={submitHandler}>
          <Link className="btn-back" to="/login">
            <i class="fas fa-arrow-left"></i>
          </Link>
          <h3 className="title">Xác thực OTP</h3>
          <p>
            Vui lòng nhập mã OTP vừa được gửi đến email your_email@yopmail.com
          </p>
          <div className="otp">
            <input
              type="text"
              maxLength="1"
              required
              onChange={(e) => setFirstNumber(e.target.value)}
            />
            <input
              type="text"
              maxLength="1"
              required
              onChange={(e) => setSecondNumber(e.target.value)}
            />
            <input
              type="text"
              maxLength="1"
              required
              onChange={(e) => setThirdNumber(e.target.value)}
            />
            <input
              type="text"
              maxLength="1"
              required
              onChange={(e) => setForthNumber(e.target.value)}
            />
            <input
              type="text"
              maxLength="1"
              required
              onChange={(e) => setFifthNumber(e.target.value)}
            />
          </div>
          <button className="btn-submit" type="submit">
            Gửi
          </button>
          <div className="resend-otp">
            Bạn chưa nhận được mã OTP?{" "}
            <span className="resend">
              Gửi lại (25s)
            </span>
          </div>
        </form>
      </div>
    </div>
  );
};

export default OtpConfirm;
