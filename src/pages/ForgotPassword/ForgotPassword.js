import React, { useState, useEffect } from 'react'
import './ForgotPassword.scss'
import { Link, useHistory, useLocation } from "react-router-dom";
import axios from 'axios';

const ForgotPassword = () => {
    const [email, setEmail] = useState('');
    const [data, setData] = useState();

      const location = useLocation();
      const history = useHistory();

      useEffect(() => {
        if (data) {
          history.push(location.search + '/otp-confirm');
        }
      }, [location, history, data]);

      const validEmail = new RegExp(
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
      );

      const validateEmail = () => {
        if (validEmail.test(email)) {
          return true;
        } else {
          return false;
        }
      };

    const submitHandler = e => {
        e.preventDefault();
        if(!validateEmail()) console.log('Invalid email');
        const config = {
          headers: {
            "Content-Type": "application/json",
          },
        };
        axios.post(
          "https://api.v2-dev.thuocsi.vn/interview/account/forgot-password/otp",
          { email },
          config
        ).then(res => {
            console.log(res.data);
            setData(res.data);
            console.log(data)
        }).catch(err => {
            console.log(err);
        });
    }
    return (
      <div className="container">
        <div className="forgot-password">
          <form className="form" onSubmit={submitHandler}>
            <Link className="btn-back" to="/login">
              <i class="fas fa-arrow-left"></i>
            </Link>
            <h3 className="title">Quên mật khẩu</h3>
            <p>Vui lòng nhập email để nhận nhận mã xác thực</p>
            <label>Email</label>
            <input type="text" placeholder="Nhập email" required onChange={e=>setEmail(e.target.value)} />
            <button className="btn-submit" type="submit">
              Gửi
            </button>
          </form>
        </div>
      </div>
    );
}

export default ForgotPassword
