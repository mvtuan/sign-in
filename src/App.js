import './App.css';
import { BrowserRouter, Route } from 'react-router-dom'
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import Homepage from './pages/Homepage/Homepage';
import RegisterSuccess from './pages/RegisterSuccess/RegisterSuccess';
import ForgotPassword from './pages/ForgotPassword/ForgotPassword';
import ResetPassword from './pages/ResetPassword/ResetPassword';
import ResetPasswordSuccess from './pages/ResetPasswordSuccess/ResetPasswordSuccess';
import OtpConfirm from './pages/OtpConfirm/OtpConfirm';

function App() {
  return (
    <BrowserRouter>
      <Route path='/' exact component={Homepage} />
      <Route path='/login' component={Login} />
      <Route path='/register' component={Register} />
      <Route path='/forgot-password' component={ForgotPassword} />
      <Route path='/register-success' component={RegisterSuccess} />
      <Route path='/reset-password' component={ResetPassword} />
      <Route path='/reset-success' component={ResetPasswordSuccess} />
      <Route path='/otp-confirm' component={OtpConfirm} />
    </BrowserRouter>
  );
}

export default App;
