import React from 'react'
import './Header.scss'
import {Link} from 'react-router-dom'

const Header = () => {
    return (
      <div className="navbar">
        <div className="navbar__brand">
          <Link className="link" to="/">
            Home
          </Link>
        </div>
        <form className="navbar__search">
          <input type="text" placeholder="Search" />
          <button type="submit" className='btn-search'>
            <i class="fas fa-search"></i>
          </button>
        </form>
        <div className="navbar__nav">
          <Link className="link" to="/login">
            Login
          </Link>
          <Link className="link" to="/register">
            Register
          </Link>
        </div>
      </div>
    );
}

export default Header
